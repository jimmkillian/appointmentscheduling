﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AppointmentScheduling.Migrations
{
    public partial class changedIDtoId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PatientID",
                table: "Appointments",
                newName: "PatientId");

            migrationBuilder.RenameColumn(
                name: "DoctorID",
                table: "Appointments",
                newName: "DoctorId");

            migrationBuilder.RenameColumn(
                name: "AdminID",
                table: "Appointments",
                newName: "AdminId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Appointments",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "Appointments",
                newName: "PatientID");

            migrationBuilder.RenameColumn(
                name: "DoctorId",
                table: "Appointments",
                newName: "DoctorID");

            migrationBuilder.RenameColumn(
                name: "AdminId",
                table: "Appointments",
                newName: "AdminID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Appointments",
                newName: "ID");
        }
    }
}
